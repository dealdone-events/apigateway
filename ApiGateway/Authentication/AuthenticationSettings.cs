﻿using System;

namespace ApiGateway.Authentication
{
    public static class AuthenticationSettings
    {
        public static string SchemeName => "AuthServiceScheme";
        
        public static string TokenHeaderName => "token";

        public static string HttpClientName => "authenticate-client";

        public static Uri AuthUrl => new("http://auth:9005");
    }
}