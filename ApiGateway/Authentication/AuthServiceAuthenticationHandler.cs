﻿using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using ApiGateway.Services;
using CommonLibraries;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace ApiGateway.Authentication
{
    public class AuthServiceAuthenticationHandler : AuthenticationHandler<AuthServiceAuthenticationSchemeOptions>
    {
        public AuthServiceAuthenticationHandler(
            TokenCacheService tokenCacheService,
            IHttpClientFactory httpClientFactory,
            IOptionsMonitor<AuthServiceAuthenticationSchemeOptions> options,
            ILoggerFactory logger,
            UrlEncoder encoder,
            ISystemClock clock) : base(options, logger, encoder, clock)
        {
            _tokenCacheService = Check.NotNull(tokenCacheService, nameof(tokenCacheService));
            _httpClientFactory = Check.NotNull(httpClientFactory, nameof(httpClientFactory));
        }

        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            if (!Request.Headers.ContainsKey(AuthenticationSettings.TokenHeaderName))
            {
                return AuthenticateResult.Fail("Header not found");
            }

            string token = Request.Headers[AuthenticationSettings.TokenHeaderName];

            if (await _tokenCacheService.HasToken(token))
            {
                return ReturnSuccess();
            }
            
            var httpClient = _httpClientFactory.CreateClient(AuthenticationSettings.HttpClientName);
            var request = new HttpRequestMessage(HttpMethod.Get, "api/auth/validate-token");
            request.Headers.Add(AuthenticationSettings.TokenHeaderName, token);

            var response = await httpClient.SendAsync(request);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                await _tokenCacheService.CacheToken(token);
                return ReturnSuccess();
            }

            return AuthenticateResult.Fail("Token is invalid");
        }

        private AuthenticateResult ReturnSuccess()
        {
            var claimsIdentity = new ClaimsIdentity(nameof(AuthServiceAuthenticationHandler));
            return AuthenticateResult.Success(new AuthenticationTicket(new ClaimsPrincipal(claimsIdentity), Scheme.Name));
        }

        private readonly IHttpClientFactory _httpClientFactory;
        private readonly TokenCacheService _tokenCacheService;
    }
}