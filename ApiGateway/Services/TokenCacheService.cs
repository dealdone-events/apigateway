﻿using System;
using System.Threading.Tasks;
using ApiGateway.ApplicationConfiguration;
using CommonLibraries;
using StackExchange.Redis;

namespace ApiGateway.Services
{
    public class TokenCacheService
    {
        public TokenCacheService(IConnectionMultiplexer redis, RedisSettings redisSettings)
        {
            _redis = Check.NotNull(redis, nameof(redis));
            _redisSettings = Check.NotNull(redisSettings, nameof(redisSettings));
        }

        public async Task CacheToken(string token)
        {
            var db = _redis.GetDatabase();

            if (db.SetLength(new RedisKey(_redisSettings.TokensRepositoryName)) == _redisSettings.CacheClearLength)
            {
                db.KeyDelete(new RedisKey(_redisSettings.TokensRepositoryName));
            }
                
            await db.SetAddAsync(new RedisKey(_redisSettings.TokensRepositoryName), new RedisValue(token));
        }

        public async Task<bool> HasToken(string token)
        {
            var db = _redis.GetDatabase();
            return await db.SetContainsAsync(new RedisKey(_redisSettings.TokensRepositoryName), new RedisValue(token));
        }

        private readonly IConnectionMultiplexer _redis;

        private readonly RedisSettings _redisSettings;
    }
}