using ApiGateway.ApplicationConfiguration;
using ApiGateway.Authentication;
using ApiGateway.Services;
using CommonLibraries;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Ocelot.DependencyInjection;
using Ocelot.Middleware;
using RockLib.Configuration.ObjectFactory;
using StackExchange.Redis;

namespace ApiGateway
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            _configuration = Check.NotNull(configuration, nameof(configuration));
        }
        
        public void ConfigureServices(IServiceCollection services)
        {
            var settings = _configuration.Create<ApplicationSettings>();

            services.AddHttpClient(AuthenticationSettings.HttpClientName)
                .ConfigureHttpClient(client => client.BaseAddress = AuthenticationSettings.AuthUrl);
            
            services.AddAuthentication()
                .AddScheme<AuthServiceAuthenticationSchemeOptions, AuthServiceAuthenticationHandler>(AuthenticationSettings.SchemeName, _ => { });

            services.AddControllers();
            services.AddSwaggerForOcelot(_configuration);

            services.AddOcelot();
            services.AddLogging();

            services.AddSingleton(settings.RedisSettings);
            services.AddSingleton<IConnectionMultiplexer>(_ => ConnectionMultiplexer.Connect(
                new ConfigurationOptions
                {
                    EndPoints = {settings.RedisSettings.ConnectionString}
                }));
            services.AddSingleton<TokenCacheService>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwaggerForOcelotUI();

            app.UseOcelot().Wait();
        }

        private readonly IConfiguration _configuration;
    }
}