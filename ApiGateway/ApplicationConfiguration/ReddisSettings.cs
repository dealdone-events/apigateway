﻿using CommonLibraries;

namespace ApiGateway.ApplicationConfiguration
{
    public class RedisSettings
    {
        public RedisSettings(string connectionString, string tokensRepositoryName, long cacheClearLength)
        {
            ConnectionString = Check.NotNullOrEmpty(connectionString, nameof(connectionString));
            TokensRepositoryName = Check.NotNullOrEmpty(tokensRepositoryName, nameof(tokensRepositoryName));
            CacheClearLength = cacheClearLength;
        }

        public long CacheClearLength { get; }
        public string TokensRepositoryName { get; }
        public string ConnectionString { get; }
    }
}