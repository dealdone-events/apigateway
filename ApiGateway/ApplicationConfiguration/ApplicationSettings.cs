﻿using CommonLibraries;

namespace ApiGateway.ApplicationConfiguration
{
    public class ApplicationSettings
    {
        public ApplicationSettings(RedisSettings redisSettings)
        {
            RedisSettings = Check.NotNull(redisSettings, nameof(redisSettings));
        }

        public RedisSettings RedisSettings { get; }
    }
}